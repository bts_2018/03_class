import { Component } from '@angular/core';
import { Animal } from './classes/animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[]
})
export class AppComponent { 
  
  age:number;
  name:string;
  
  constructor() {
    var x = new Animal("Bob",6);
    this.name  = x.name;
    this.age  = x.grow();
   
  }  
 
}
