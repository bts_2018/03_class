export class Animal { 
	//attribute 
	name:string;
	age:number; 

	//constructor 
	constructor(name:string,age:number) { 
	   this.name = name;
	   this.age = age;
	}  
	
	//method 
	grow():number { 
		return this.age + 1;
	} 
 } 